Name:           suricata
Version:        4.0.5
Release:        1%{?dist}
Summary:        Intrusion Detection System
Group:		Applications/Internet
License:        GPLv2
URL:            https://suricata-ids.org
Source0:        suricata-4.0.5.tar.gz
Source1:	suricata.service
Source2:	suricata.sysconfig
Source3:	suricata.logrotate

BuildRequires: gcc gcc-c++ libyaml-devel zlib-devel libpcap-devel pcre-devel libcap-ng-devel
BuildRequires: file-devel GeoIP-devel lua-devel autoconf automake libtool systemd
BuildRequires: libevent-devel libprelude-devel 
Requires(pre): /sbin/useradd 
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
       

%description
The Suricata Engine is an Open Source Next Generation Intrusion
Detection and Prevention Engine. This engine is not intended to 
just replace or emulate the existing tools in the industry, but
will bring new ideas and technologies to the field. This new Engine
supports Multi-threading, Automatic Protocol Detection (IP, TCP,
UDP, ICMP, HTTP, TLS, FTP and SMB!), Gzip Decompression, Fast IP
Matching, and GeoIP identification. 


%prep
%setup -q


%build
%configure --enable-af-packet --enable-geoip --enable-lua
#make %{?_smp_mflags}

%make_build

%install
#rm -rf $RPM_BUILD_ROOT
#%make_install

make DESTDIR="%{buildroot}" "bindir=%{_sbindir}" install

#setup /etc directory
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/rules
install -m 600 rules/*.rules %{buildroot}%{_sysconfdir}/%{name}/rules
install -m 600 *.config %{buildroot}%{_sysconfdir}/%{name}
install -m 600 suricata.yaml %{buildroot}%{_sysconfdir}/%{name}
mkdir -p %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
install -m 0755 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/%{name}

#set up logging
mkdir -p %{buildroot}/data/suricata/log
mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d
install -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}

#removing things so they don't get picked up
rm -rf %{buildroot}%{_includedir}
rm -rf %{buildroot}%{_libdir}/libhtp.la
rm -rf %{buildroot}%{_libdir}/libhtp.a
rm -rf %{buildroot}%{_libdir}/libhtp.so
rm -rf %{buildroot}%{_libdir}/pkgconfig

%check
make check

%pre
getent passwd suricata >/dev/null || useradd -r -M -s /sbin/nologin suricata

%post
/sbin/ldconfig

%preun
%systemd_post suricata.service

%postun
/sbin/ldconfig
%systemd_postun_with_restart suricata.service

#there are easier ways to do the files section. I am a caveman so all are listed
%files
%dir /data/suricata/log
%doc
/etc/logrotate.d/suricata
/etc/suricata/classification.config
/etc/suricata/reference.config
/etc/suricata/rules/app-layer-events.rules
/etc/suricata/rules/decoder-events.rules
/etc/suricata/rules/dnp3-events.rules
/etc/suricata/rules/dns-events.rules
/etc/suricata/rules/files.rules
/etc/suricata/rules/http-events.rules
/etc/suricata/rules/modbus-events.rules
/etc/suricata/rules/nfs-events.rules
/etc/suricata/rules/ntp-events.rules
/etc/suricata/rules/smtp-events.rules
/etc/suricata/rules/stream-events.rules
/etc/suricata/rules/tls-events.rules
/etc/suricata/suricata.yaml
/etc/suricata/threshold.config
/etc/sysconfig/suricata
/usr/bin/suricatasc
/usr/lib/python2.7/site-packages/suricatasc-0.9-py2.7.egg-info
/usr/lib/python2.7/site-packages/suricatasc/__init__.py
/usr/lib/python2.7/site-packages/suricatasc/__init__.pyc
/usr/lib/python2.7/site-packages/suricatasc/__init__.pyo
/usr/lib/python2.7/site-packages/suricatasc/suricatasc.py
/usr/lib/python2.7/site-packages/suricatasc/suricatasc.pyc
/usr/lib/python2.7/site-packages/suricatasc/suricatasc.pyo
/usr/lib/systemd/system/suricata.service
/usr/lib64/libhtp.so.2
/usr/lib64/libhtp.so.2.0.0
/usr/sbin/suricata
/usr/share/doc/suricata/AUTHORS
/usr/share/doc/suricata/Basic_Setup.txt
/usr/share/doc/suricata/CentOS5.txt
/usr/share/doc/suricata/CentOS_56_Installation.txt
/usr/share/doc/suricata/Debian_Installation.txt
/usr/share/doc/suricata/Fedora_Core.txt
/usr/share/doc/suricata/FreeBSD_8.txt
/usr/share/doc/suricata/GITGUIDE
/usr/share/doc/suricata/HTP_library_installation.txt
/usr/share/doc/suricata/INSTALL
/usr/share/doc/suricata/INSTALL.PF_RING
/usr/share/doc/suricata/INSTALL.WINDOWS
/usr/share/doc/suricata/Installation_from_GIT_with_PCRE-JIT.txt
/usr/share/doc/suricata/Installation_from_GIT_with_PF_RING_on_Ubuntu_server_1104.txt
/usr/share/doc/suricata/Installation_with_CUDA_and_PFRING_on_Scientific_Linux_6.txt
/usr/share/doc/suricata/Installation_with_CUDA_and_PF_RING_on_Ubuntu_server_1104.txt
/usr/share/doc/suricata/Installation_with_CUDA_on_Scientific_Linux_6.txt
/usr/share/doc/suricata/Installation_with_CUDA_on_Ubuntu_server_1104.txt
/usr/share/doc/suricata/Installation_with_PF_RING.txt
/usr/share/doc/suricata/Mac_OS_X_106x.txt
/usr/share/doc/suricata/NEWS
/usr/share/doc/suricata/OpenBSD_Installation_from_GIT.txt
/usr/share/doc/suricata/README
/usr/share/doc/suricata/Setting_up_IPSinline_for_Linux.txt
/usr/share/doc/suricata/TODO
/usr/share/doc/suricata/Third_Party_Installation_Guides.txt
/usr/share/doc/suricata/Ubuntu_Installation.txt
/usr/share/doc/suricata/Ubuntu_Installation_from_GIT.txt
/usr/share/doc/suricata/Windows.txt
/usr/share/man/man1/suricata.1.gz

%changelog
* Tue Oct 9 2018 Ronnie Grubbs <grubbs.ronnie@gmail.com> - 4.0.5-1
- first build attempt which was successful

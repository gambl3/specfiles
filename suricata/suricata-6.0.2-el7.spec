Name:           suricata
Version:        6.0.2
Release:        1%{?dist}
Summary:        Intrusion Detection System
Group:		Applications/Internet
License:        GPLv2
URL:            https://suricata-ids.org
Source0:        suricata-6.0.2.tar.gz
Source1:	suricata.service
Source2:	suricata.sysconfig
Source3:	suricata.logrotate

BuildRequires: gcc gcc-c++ libyaml-devel zlib-devel 
BuildRequires: libpcap-devel pcre-devel libcap-ng-devel rust
BuildRequires: file-devel GeoIP-devel lua-devel autoconf
BuildRequires: automake libtool systemd libmaxminddb-devel nss-devel nspr-devel
BuildRequires: libevent-devel libprelude-devel 
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
       
%description
The Suricata Engine is an Open Source Next Generation Intrusion
Detection and Prevention Engine. This engine is not intended to 
just replace or emulate the existing tools in the industry, but
will bring new ideas and technologies to the field. This new Engine
supports Multi-threading, Automatic Protocol Detection (IP, TCP,
UDP, ICMP, HTTP, TLS, FTP and SMB!), Gzip Decompression, Fast IP
Matching, and GeoIP identification. 

%prep
%setup -q

%build
%configure --enable-af-packet --enable-geoip --enable-lua
make %{?_smp_mflags}

%make_build

%install
rm -rf $RPM_BUILD_ROOT
%make_install

make DESTDIR="%{buildroot}" "bindir=%{_sbindir}" install

#setup /etc directory
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/rules
install -m 600 rules/*.rules %{buildroot}%{_sysconfdir}/%{name}/rules
install -m 600 *.config %{buildroot}%{_sysconfdir}/%{name}
install -m 600 suricata.yaml %{buildroot}%{_sysconfdir}/%{name}
mkdir -p %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
install -m 0755 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/%{name}

#set up logging
mkdir -p %{buildroot}/data/suricata/logs
mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d
install -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}

#removing things so they don't get picked up
rm -rf %{buildroot}%{_includedir}
rm -rf %{buildroot}%{_libdir}/libhtp.la
rm -rf %{buildroot}%{_libdir}/libhtp.a
rm -rf %{buildroot}%{_libdir}/libhtp.so
rm -rf %{buildroot}%{_libdir}/pkgconfig

%check
make check

%pre
getent passwd suricata >/dev/null || useradd -r -M -s /sbin/nologin suricata

%post
/sbin/ldconfig

%preun
%systemd_post suricata.service

%postun
/sbin/ldconfig
%systemd_postun_with_restart suricata.service

%files
%dir /data/suricata/logs
/etc/sysconfig/suricata
/usr/bin/suricata-update
/usr/bin/suricatactl
/usr/bin/suricatasc
/etc/logrotate.d/suricata
/etc/suricata/
/usr/share/
/usr/sbin/suricata
/usr/lib/systemd/system/suricata.service
/usr/lib64/libhtp.so.2
/usr/lib64/libhtp.so.2.0.0
/usr/bin/suricata
/usr/lib/python2.7/site-packages/suricata-6.0.2-py2.7.egg-info
/usr/lib/python2.7/site-packages/

%changelog
* Thu Apr 22 2021 Ronnie Grubbs <grubbs.ronnie@gmail> 6.0.2
- Updated the version to 6.0.2 for el7
* Wed Jul 17 2019 Ronnie Grubbs <grubbs.ronnie@gmail> 4.1.4
- Updated the version to 4.1.4

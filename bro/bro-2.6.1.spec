Name:           bro
Version:        2.6.1
Release:        1%{?dist}
Summary: 	A Network Intrusion Detection System and Analysis Framework       

License:        BSD
URL:            https://bro.org

Source0:        bro-2.6.1.tar.gz
Source1:        bro.service
Source2:	bro-af_packet-plugin.tar.gz

BuildRequires:  cmake libpcap-devel openssl-devel zlib-devel ncurses-devel curl-devel libtool byacc swig bison flex file-devel libxml2-devel readline-devel libmaxminddb-devel

BuildRequires:    gperftools-devel bind-devel jemalloc-devel python2-devel python-tools GeoIP-devel systemd

Requires:    pysubnettree trace-summary capstats libmaxminddb
Requires(post):   systemd
Requires(preun):  systemd 
Requires(postun): systemd

%description
Zeek is an open-source, Unix-based Network Intrusion Detection System (NIDS)
that passively monitors network traffic and looks for suspicious activity.
Zeek detects intrusions by first parsing network traffic to extract is
application-level semantics and then executing event-oriented analyzers that
compare the activity with patterns deemed troublesome. Its analysis includes
detection of specific attacks (including those defined by signatures, but also
those defined in terms of events) and unusual activities (e.g., certain hosts
connecting to certain services, or patterns of failed connection attempts).

%define broctl_logdir /data/bro/logs
%define broctl_spooldir /data/bro/spool
%define plugin_root /opt/bro/lib/bro/plugins

%package plugin-afpacket
Summary: AF_Packet plugin for Bro

BuildRequires: kernel-devel
Requires: bro = %{version}-%{release}

%description plugin-afpacket
This package contains the AF_Packet plugin for bro. This allows Linux instances
of Bro to use the native kernerl interface to perform memory-mapped (i.e. zero
copy) packet capture int Bro. One limitation of this method is that VLAN tags are
lost during the process.

%prep
%setup -q

#move to directory with af_packet plugin file
pushd aux
tar xvf %{SOURCE2}
popd


%build
./configure --prefix=/opt/bro --spooldir=%{broctl_spooldir} --logdir=%{broctl_logdir} --enable-mobile-ipv6 --enable-jemalloc --enable-broccoli

make %{?_smp_mflags}

#make af_packet plugin
pushd aux/bro-af_packet-plugin-master
./configure --bro-dist=%{_builddir}/bro-2.6.1
make %{?_smp_mflags}
popd


%install
make install DESTDIR=%{buildroot} INSTALL="install -p"

#install service file
%{__install} -D -c -m 644 %{SOURCE1} %{buildroot}%{_unitdir}/bro.service

#setup logging directories
mkdir -p %{buildroot}/data/bro/logs
mkdir -p %{buildroot}/data/bro/spool
mkdir -p %{buildroot}/data/bro/pem

#install the AF_Packet plugin
pushd aux/bro-af_packet-plugin-master
#make install
#make install DESTDIR=%{buildroot}/opt/bro/lib/bro/plugins
make install DESTDIR=%{buildroot}
popd


%post
/sbin/ldconfig
%systemd_post bro.service

#create symbolic links 
ln -sf /opt/bro/bin/bro %{_bindir}
ln -sf /opt/bro/bin/bifcl %{_bindir}
ln -sf /opt/bro/bin/broctl %{_bindir}
ln -sf /opt/bro/bin/bro-cut %{_bindir}
ln -sf /opt/bro/bin/capstats %{_bindir}
ln -sf /opt/bro/bin/broccoli %{_bindir}
ln -sf /opt/bro/bin/bro-config %{_bindir}
ln -sf /opt/bro/bin/trace-summary %{_bindir}

%preun
%systemd_preun bro.service

%postun
%systemd_postun bro.service

%check
#make test

%files plugin-afpacket
/opt/bro/lib/bro/plugins/Bro_AF_Packet

%files
%dir /data/bro/logs
%dir /data/bro/spool
%dir /data/bro/pem
/opt/bro/bin
%config /opt/bro/etc/broctl.cfg
%config /opt/bro/etc/node.cfg
%config /opt/bro/etc/broccoli.conf
%config /opt/bro/etc/networks.cfg
/opt/bro/include
/opt/bro/lib
/opt/bro/share
/usr/lib/systemd/system/bro.service
/data/bro/spool/broctl-config.sh

%changelog
* Thu Feb 7 2019 Ronnie Grubbs <grubbs.ronnie@gmail> 2.6.1
- Added the af-packet plugin RPM build to this package

* Mon Jan 28 2019 Ronnie Grubbs <grubbs.ronnie@gmail> 2.6.1
- Update to latest version of zeek/bro for 23 N-CPT

# Spec File Repository
Custom spec files for CentOS 7

## Suricata
Builds the Suricata IDS with af-packet enabled. 

## Zeek
Builds the open source Zeek IDS, formerly known as bro. Also build af-packet plugin.

## Bro
Old repository for the Bro IDS with af-packet support.